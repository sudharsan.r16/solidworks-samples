using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Reflection;

using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swpublished;
using SolidWorks.Interop.swconst;
using SolidWorksTools.File;


namespace qwe {

  /// <summary>
  /// Summary description for qwe.
  /// </summary>
  [Guid( "7e5e69fc-ce68-47ee-b3ae-eac40a348327" ), ComVisible( true )]
  [SwAddin(
      Description = "qwe description",
      Title = "qwe",
      LoadAtStartup = true
      )]
  public class SwAddin: ISwAddin {
    #region Local Variables
    ISldWorks iSwApp;
    ICommandManager iCmdMgr;
    int addinID;

    #region Event Handler Variables
    Hashtable openDocs;
    SldWorks SwEventPtr;
    #endregion

    #region Property Manager Variables
    UserPMPage ppage;
    #endregion


    // Public Properties
    public ISldWorks SwApp {
      get { return iSwApp; }
    }
    public ICommandManager CmdMgr {
      get { return iCmdMgr; }
    }

    #endregion

    #region SolidWorks Registration
    [ComRegisterFunctionAttribute]
    public static void RegisterFunction( Type t ) {

      #region Get Custom Attribute: SwAddinAttribute
      SwAddinAttribute SWattr = null;
      Type type = typeof( SwAddin );
      foreach ( System.Attribute attr in type.GetCustomAttributes( false ) ) {
        if ( attr is SwAddinAttribute ) {
          SWattr = attr as SwAddinAttribute;
          break;
        }
      }
      #endregion

      Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
      Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

      string keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
      Microsoft.Win32.RegistryKey addinkey = hklm.CreateSubKey( keyname );
      addinkey.SetValue( null, 0 );

      addinkey.SetValue( "Description", SWattr.Description );
      addinkey.SetValue( "Title", SWattr.Title );

      keyname = "Software\\SolidWorks\\AddInsStartup\\{" + t.GUID.ToString() + "}";
      addinkey = hkcu.CreateSubKey( keyname );
      addinkey.SetValue( null, Convert.ToInt32( SWattr.LoadAtStartup ) );
    }

    [ComUnregisterFunctionAttribute]
    public static void UnregisterFunction( Type t ) {
      Microsoft.Win32.RegistryKey hklm = Microsoft.Win32.Registry.LocalMachine;
      Microsoft.Win32.RegistryKey hkcu = Microsoft.Win32.Registry.CurrentUser;

      string keyname = "SOFTWARE\\SolidWorks\\Addins\\{" + t.GUID.ToString() + "}";
      hklm.DeleteSubKey( keyname );

      keyname = "Software\\SolidWorks\\AddInsStartup\\{" + t.GUID.ToString() + "}";
      hkcu.DeleteSubKey( keyname );
    }

    #endregion

    #region ISwAddin Implementation
    public SwAddin() {
    }

    public bool ConnectToSW( object ThisSW, int cookie ) {
      iSwApp = (ISldWorks) ThisSW;
      addinID = cookie;

      //Setup callbacks
      iSwApp.SetAddinCallbackInfo( 0, this, addinID );

      #region Setup the Command Manager
      iCmdMgr = iSwApp.GetCommandManager( cookie );
      AddCommandMgr();
      #endregion

      #region Setup the Event Handlers
      SwEventPtr = (SldWorks) iSwApp;
      openDocs = new Hashtable();
      AttachEventHandlers();
      #endregion

      #region Setup Sample Property Manager
      AddPMP();
      #endregion

      return true;
    }

    public bool DisconnectFromSW() {
      RemoveCommandMgr();
      RemovePMP();
      DetachEventHandlers();

      iSwApp = null;
      //The addin _must_ call GC.Collect() here in order to retrieve all managed code pointers 
      GC.Collect();
      return true;
    }
    #endregion

    #region UI Methods
    public void AddCommandMgr() {
      ICommandGroup cmdGroup;
      BitmapHandler iBmp = new BitmapHandler();
      Assembly thisAssembly;

      thisAssembly = System.Reflection.Assembly.GetAssembly( this.GetType() );

      cmdGroup = iCmdMgr.CreateCommandGroup( 1, "C# Addin", "C# Addin", "", -1 );
      cmdGroup.LargeIconList = iBmp.CreateFileFromResourceBitmap( "qwe.ToolbarLarge.bmp", thisAssembly );
      cmdGroup.SmallIconList = iBmp.CreateFileFromResourceBitmap( "qwe.ToolbarSmall.bmp", thisAssembly );
      cmdGroup.LargeMainIcon = iBmp.CreateFileFromResourceBitmap( "qwe.MainIconLarge.bmp", thisAssembly );
      cmdGroup.SmallMainIcon = iBmp.CreateFileFromResourceBitmap( "qwe.MainIconSmall.bmp", thisAssembly );

      cmdGroup.AddCommandItem( "CreateCube", -1, "Create a cube", "Create cube", 0, "CreateCube", "", 0 );
      cmdGroup.AddCommandItem( "Show PMP", -1, "Display sample property manager", "Show PMP", 2, "ShowPMP", "EnablePMP", 2 );

      cmdGroup.HasToolbar = true;
      cmdGroup.HasMenu = true;
      cmdGroup.Activate();

      thisAssembly = null;
      iBmp.Dispose();
    }


    public void RemoveCommandMgr() {
      iCmdMgr.RemoveCommandGroup( 1 );
    }

    public Boolean AddPMP() {
      ppage = new UserPMPage( this );
      return true;
    }

    public Boolean RemovePMP() {
      ppage = null;
      return true;
    }

    #endregion

    #region UI Callbacks
    public void CreateCube() {
      //make sure we have a part open
      string partTemplate = iSwApp.GetUserPreferenceStringValue( (int) swUserPreferenceStringValue_e.swDefaultTemplatePart );
      IModelDoc2 modDoc = (IModelDoc2) iSwApp.NewDocument( partTemplate, (int) swDwgPaperSizes_e.swDwgPaperA2size, 0.0, 0.0 );

      modDoc.InsertSketch2( true );
      modDoc.SketchRectangle( 0, 0, 0, .1, .1, .1, false );
      //Extrude the sketch
      IFeatureManager featMan = modDoc.FeatureManager;
      featMan.FeatureExtrusion( true,
          false, false,
          (int) swEndConditions_e.swEndCondBlind, (int) swEndConditions_e.swEndCondBlind,
          0.1, 0.0,
          false, false,
          false, false,
          0.0, 0.0,
          false, false,
          false, false,
          true,
          false, false );
    }


    public void ShowPMP() {
      if ( ppage != null )
        ppage.Show();
    }

    public int EnablePMP() {
      if ( iSwApp.ActiveDoc != null )
        return 1;
      else
        return 0;
    }
    #endregion

    #region Event Methods
    public bool AttachEventHandlers() {
      AttachSwEvents();
      //Listen for events on all currently open docs
      ModelDoc2 modDoc;
      modDoc = (ModelDoc2) iSwApp.GetFirstDocument();
      while ( modDoc != null ) {
        if ( !openDocs.Contains( modDoc ) ) {
          AttachModelDocEventHandler( modDoc );
        }
        modDoc = (ModelDoc2) modDoc.GetNext();
      }
      return true;
    }

    private bool AttachSwEvents() {
      try {
        SwEventPtr.ActiveDocChangeNotify += new DSldWorksEvents_ActiveDocChangeNotifyEventHandler( OnDocChange );
        SwEventPtr.DocumentLoadNotify += new DSldWorksEvents_DocumentLoadNotifyEventHandler( OnDocLoad );
        SwEventPtr.FileNewNotify2 += new DSldWorksEvents_FileNewNotify2EventHandler( OnFileNew );
        SwEventPtr.ActiveModelDocChangeNotify += new DSldWorksEvents_ActiveModelDocChangeNotifyEventHandler( OnModelChange );
        return true;
      }
      catch ( Exception e ) {
        Console.WriteLine( e.Message );
        return false;
      }
    }

    private bool DetachSwEvents() {
      try {
        SwEventPtr.ActiveDocChangeNotify -= new DSldWorksEvents_ActiveDocChangeNotifyEventHandler( OnDocChange );
        SwEventPtr.DocumentLoadNotify -= new DSldWorksEvents_DocumentLoadNotifyEventHandler( OnDocLoad );
        SwEventPtr.FileNewNotify2 -= new DSldWorksEvents_FileNewNotify2EventHandler( OnFileNew );
        SwEventPtr.ActiveModelDocChangeNotify -= new DSldWorksEvents_ActiveModelDocChangeNotifyEventHandler( OnModelChange );
        return true;
      }
      catch ( Exception e ) {
        Console.WriteLine( e.Message );
        return false;
      }

    }

    public bool AttachModelDocEventHandler( ModelDoc2 modDoc ) {
      if ( modDoc == null )
        return false;

      DocumentEventHandler docHandler = null;

      if ( !openDocs.Contains( modDoc ) ) {
        switch ( modDoc.GetType() ) {
          case (int) swDocumentTypes_e.swDocPART: {
              docHandler = new PartEventHandler( modDoc, this );
              break;
            }
          case (int) swDocumentTypes_e.swDocASSEMBLY: {
              docHandler = new AssemblyEventHandler( modDoc, this );
              break;
            }
          case (int) swDocumentTypes_e.swDocDRAWING: {
              docHandler = new DrawingEventHandler( modDoc, this );
              break;
            }
          default: {
              return false; //Unsupported document type
            }
        }
        docHandler.AttachEventHandlers();
        openDocs.Add( modDoc, docHandler );
      }
      return true;
    }

    public bool DetachModelEventHandler( ModelDoc2 modDoc ) {
      DocumentEventHandler docHandler;
      docHandler = (DocumentEventHandler) openDocs[modDoc];
      openDocs.Remove( modDoc );
      modDoc = null;
      docHandler = null;
      return true;
    }

    public bool DetachEventHandlers() {
      DetachSwEvents();

      //Close events on all currently open docs
      DocumentEventHandler docHandler;
      int numKeys = openDocs.Count;
      object[] keys = new Object[numKeys];

      //Remove all document event handlers
      openDocs.Keys.CopyTo( keys, 0 );
      foreach ( ModelDoc2 key in keys ) {
        docHandler = (DocumentEventHandler) openDocs[key];
        docHandler.DetachEventHandlers(); //This also removes the pair from the hash
        docHandler = null;
      }
      return true;
    }
    #endregion

    #region Event Handlers
    //Events
    public int OnDocChange() {
      return 0;
    }

    public int OnDocLoad( string docTitle, string docPath ) {
      ModelDoc2 modDoc = (ModelDoc2) iSwApp.GetFirstDocument();
      while ( modDoc != null ) {
        if ( modDoc.GetTitle() == docTitle ) {
          if ( !openDocs.Contains( modDoc ) ) {
            AttachModelDocEventHandler( modDoc );
          }
        }
        modDoc = (ModelDoc2) modDoc.GetNext();
      }
      return 0;
    }

    public int OnFileNew( object newDoc, int docType, string templateName ) {
      return 0;
    }

    public int OnModelChange() {
      return 0;
    }

    #endregion
  }

  #region SwAddin class attributes
  [AttributeUsage( AttributeTargets.Class )]
  public class SwAddinAttribute: System.Attribute {
    public string Description = "";
    public string Title = "";
    public bool LoadAtStartup = false;

    public SwAddinAttribute() {
    }
  }
  #endregion
}
