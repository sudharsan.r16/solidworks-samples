﻿namespace DotNetControlsDemo {
  partial class Form1 {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose( bool disposing ) {
      if ( disposing && ( components != null ) ) {
        components.Dispose();
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.button1 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // listBox1
      // 
      this.listBox1.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left )
      | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Items.AddRange( new object[] {
            "Help",
            "Whats New"} );
      this.listBox1.Location = new System.Drawing.Point( 25, 160 );
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size( 304, 43 );
      this.listBox1.TabIndex = 10;
      this.listBox1.SelectedIndexChanged += new System.EventHandler( this.listBox1_SelectedIndexChanged );
      // 
      // comboBox1
      // 
      this.comboBox1.DisplayMember = "Monday";
      this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange( new object[] {
            "Monday ",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"} );
      this.comboBox1.Location = new System.Drawing.Point( 25, 122 );
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size( 140, 21 );
      this.comboBox1.TabIndex = 9;
      this.comboBox1.ValueMember = "Monday";
      this.comboBox1.SelectedIndexChanged += new System.EventHandler( this.comboBox1_SelectedIndexChanged );
      // 
      // textBox1
      // 
      this.textBox1.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
      | System.Windows.Forms.AnchorStyles.Left )
      | System.Windows.Forms.AnchorStyles.Right ) ) );
      this.textBox1.Location = new System.Drawing.Point( 25, 86 );
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size( 321, 20 );
      this.textBox1.TabIndex = 8;
      this.textBox1.TextChanged += new System.EventHandler( this.textBox1_TextChanged );
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point( 25, 12 );
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size( 140, 57 );
      this.button1.TabIndex = 7;
      this.button1.Text = "Say Hello";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler( this.button1_Click );
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size( 382, 215 );
      this.ControlBox = false;
      this.Controls.Add( this.listBox1 );
      this.Controls.Add( this.comboBox1 );
      this.Controls.Add( this.textBox1 );
      this.Controls.Add( this.button1 );
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "Form1";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "Form1";
      this.ResumeLayout( false );
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button button1;
  }
}