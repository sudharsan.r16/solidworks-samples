using System;
using System.Windows;
using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swpublished;
using SolidWorks.Interop.swconst;
using System.Windows.Forms;

using System.Windows.Forms.Integration;

namespace DotNetControlsDemo {
  public class UserPMPage {
    //Local Objects
    IPropertyManagerPage2 swPropertyPage;
    PMPHandler handler;
    ISldWorks iSwApp;
    SwAddin userAddin;

    WPFControl MyWPFControl;
    ElementHost elhost;
    UserControl1 MyUserControl;
    Form1 MyWinFormControl;

    #region Property Manager Page Controls
    //Groups
    IPropertyManagerPageGroup group1;
    IPropertyManagerPageGroup group2;
    IPropertyManagerPageGroup group3;

    //Controls
    IPropertyManagerPageWindowFromHandle dotnet1;
    IPropertyManagerPageWindowFromHandle dotnet2;
    IPropertyManagerPageWindowFromHandle dotnet3;

    //Control IDs
    public const int group1ID = 0;
    public const int group2ID = 1;
    public const int group3ID = 2;

    public const int DotNet1ID = 3;
    public const int DotNet2ID = 4;
    public const int DotNet3ID = 5;

    #endregion

    public UserPMPage( SwAddin addin ) {
      userAddin = addin;
      iSwApp = (ISldWorks) userAddin.SwApp;
      CreatePropertyManagerPage();
    }


    protected void CreatePropertyManagerPage() {
      int errors = -1;
      int options = (int) swPropertyManagerPageOptions_e.swPropertyManagerOptions_OkayButton |
          (int) swPropertyManagerPageOptions_e.swPropertyManagerOptions_CancelButton;

      handler = new PMPHandler( userAddin );
      swPropertyPage = (IPropertyManagerPage2) iSwApp.CreatePropertyManagerPage( ".Net PMP control", options, handler, ref errors );
      if ( swPropertyPage != null && errors == (int) swPropertyManagerPageStatus_e.swPropertyManagerPage_Okay ) {
        try {
          AddControls();
        }
        catch ( Exception e ) {
          iSwApp.SendMsgToUser2( e.Message, 0, 0 );
        }
      }
    }


    //Controls display on the page top to bottom in the order 
    //in which they are added to the page
    protected void AddControls() {
      short controlType = -1;
      short align = -1;
      int options = -1;


      //Add the groups
      options = (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Expanded |
                (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Visible;

      group1 = (IPropertyManagerPageGroup) swPropertyPage.AddGroupBox( group1ID, "Windows Form controls", options );

      options = (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Expanded |
                (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Visible;

      group2 = (IPropertyManagerPageGroup) swPropertyPage.AddGroupBox( group2ID, "User controls", options );

      options = (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Checkbox |
                  (int) swAddGroupBoxOptions_e.swGroupBoxOptions_Visible;

      //group3 = (IPropertyManagerPageGroup) swPropertyPage.AddGroupBox( group3ID, "WPF controls", options );

      controlType = (int) swPropertyManagerPageControlType_e.swControlType_WindowFromHandle;
      align = (int) swPropertyManagerPageControlLeftAlign_e.swControlAlign_LeftEdge;
      options = (int) swAddControlOptions_e.swControlOptions_Enabled |
                (int) swAddControlOptions_e.swControlOptions_Visible;

      dotnet1 = (IPropertyManagerPageWindowFromHandle) group1.AddControl2( DotNet1ID, controlType, ".Net Windows Form control", align, options, "This control is added without COM" );
      dotnet1.Height = 150;

      controlType = (int) swPropertyManagerPageControlType_e.swControlType_WindowFromHandle;
      align = (int) swPropertyManagerPageControlLeftAlign_e.swControlAlign_LeftEdge;
      options = (int) swAddControlOptions_e.swControlOptions_Enabled |
                (int) swAddControlOptions_e.swControlOptions_Visible;

      dotnet2 = (IPropertyManagerPageWindowFromHandle) group2.AddControl2( DotNet2ID, controlType, ".Net user form control", align, options, "This control is added without COM" );
      dotnet2.Height = 150;

      dotnet3 = (IPropertyManagerPageWindowFromHandle) group3.AddControl2( DotNet3ID, controlType, ".Net WPF control", align, options, "This control is added without COM" );
      dotnet3.Height = 50;


    }

    public void Show() {


      //Windows Form control
      MyWinFormControl = new Form1();
      //If you are adding Windows form in the PropertyManager Page, set TopLevel to false
      MyWinFormControl.TopLevel = false;
      MyWinFormControl.Show();
      dotnet1.SetWindowHandlex64( MyWinFormControl.Handle.ToInt64() );


      //User control
      MyUserControl = new UserControl1();
      dotnet2.SetWindowHandlex64( MyUserControl.Handle.ToInt64() );

      ////WPF control
      elhost = new ElementHost();
      MyWPFControl = new WPFControl();
      elhost.Child = MyWPFControl;
      dotnet3.SetWindowHandlex64( elhost.Handle.ToInt64() );

      //Show PropertyManager page
      swPropertyPage.Show();

    }
  }
}